#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "planned" #:version apt-version]{Planned Features}

This chapter describes features that are planned for @(tb) or under development:
please @seclink["getting-help"]{contact us} if you have any feedback or
suggestions!

@apt-only{
    @section[#:tag "planned-virt-switching"]{Simpler Virtual/Physical Profile Switching}

    As recommended in the chapter about
    @seclink["repeatable-research"]{repeatable research}, moving back and forth
    between @seclink["virtual-machines"]{virtual} and
    @seclink["physical-machines"]{physical} resources is a good way to move
    from doing preliminary work to gathering final results for publication.
    Doing this currently requires making two profiles. We plan to make changes
    to the @(tb) interface to allow users to select, at experiment creation
    time, whether to use virtual or physical resources.
}

@section[#:tag "planned-physical-display"]{Improved Physical Resource Descriptions}

As part of the process of reserving resources on @(tb), a type of
@seclink["rspecs"]{RSpec} called a
@hyperlink["http://groups.geni.net/geni/wiki/GENIExperimenter/RSpecs#ManifestRSpec"]{manifest}
is created. The manifest gives a detailed description of the hardware
allocated, including the specifics of network topology. Currently, @(tb) does
not directly export this information to the user. In the interest of improving
transparency and @seclink["repeatable-research"]{repeatable research}, @(tb)
will develop interfaces to expose, explore, and export this information.

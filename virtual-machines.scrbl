#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "virtual-machines-advanced" #:style main-style #:version apt-version]{Virtual Machines and Containers}

A @(tb) virtual node is a virtual machine or container running on top of
a regular operating system. @(tb) virtual nodes are based on the
@seclink["xen-virtual-machines"]{Xen hypervisor} or on
@seclink["docker-containers"]{Docker containers}. Both types of
virtualization allow groups of processes to be isolated from each other
while running on the same physical machine. @(tb) virtual nodes provide
isolation of the filesystem, process, network, and account
namespaces. Thus, each virtual node has its own private filesystem,
process hierarchy, network interfaces and IP addresses, and set of users
and groups. This level of virtualization allows unmodified applications
to run as though they were on a real machine. Virtual network interfaces
support an arbitrary number of virtual network links. These links may be
individually shaped according to user-specified link parameters, and may
be multiplexed over physical links or used to connect to virtual nodes
within a single physical node.

There are a few specific differences between virtual and physical nodes.
First, @(tb) physical nodes have a routable, public IPv4 address
allowing direct remote access (unless the @(tb) installation has been
configured to use unroutable control network IP addresses, which is very
rare).  However, virtual nodes are assigned control network IP addresses
on a private network (typically the @tt{172.16/12} subnet) and are
remotely accessible over ssh via DNAT (destination network-address
translation) to the physical host's public control network IP address,
to a high-numbered port.  Depending on local configuration, it may be
possible to @seclink["public-ip-access"]{request routable IP addresses}
for specific virtual nodes to enable direct remote access.  Note that
virtual nodes are always able to access the public Internet via SNAT
(source network-address translation; nearly identical to masquerading).

Second, virtual nodes and their virtual network interfaces are connected
by virtual links built atop physical links and physical interfaces.  The
virtualization of a physical device/link decreases the fidelity of the
network emulation.  Moreover, several virtual links may share the same
physical links via multiplexing.  Individual links are isolated at layer
2, but they are not isolated in terms of performance.  If you request a
specific bandwidth for a given set of links, our resource mapper will
ensure that if multiple virtual links are mapped to a single physical
link, the sum of the bandwidths of the virtual links will not exceed the
capacity of the physical link (unless you also specify that this
constraint can be ignored by setting the @tt{best_effort} link parameter to
@tt{True}).  For example, no more than ten 1Gbps virtual links can be
mapped to a 10Gbps physical link.

Finally, when you allocate virtual nodes, you can specify the amount of
CPU and RAM (and, for Xen VMs, virtual disk space) each node will be
allocated.  @(tb)'s resource assigner will not oversubscribe these quantities.


@section[#:tag "xen-virtual-machines"]{Xen VMs}

These examples show the basics of allocating Xen VMs:
@seclink["geni-lib-example-single-vm"]{a single Xen VM node},
@seclink["geni-lib-example-two-vm-lan"]{two Xen VMs in a LAN},
@seclink["geni-lib-example-single-vm-sized"]{a Xen VM with custom disk size}.
In the sections below, we discuss advanced Xen VM allocation features.

@subsection[#:tag "xen-cores-ram"]{Controlling CPU and Memory}

You can control the number of cores and the amount of memory allocated
to each VM by setting the @tt{cores} and @tt{ram} instance variables of
a @tt{XenVM} object, as shown in the following example:

@code-sample["geni-lib-xen-cores-ram.py"]

@subsection[#:tag "xen-extrafs"]{Controlling Disk Space}

Each Xen VM is given enough disk space to hold the requested image.
Most @(tb) images are built with a 16 GB root partition, typically with
about 25% of the disk space used by the operating system.  If the
remaining space is not enough for your needs, you can request additional
disk space by setting a @tt{XEN_EXTRAFS} node attribute, as shown in the
following example.

@code-sample["geni-lib-xen-extrafs.py"]

This attribute's unit is in GB.  As with @(tb) physical nodes, the extra
disk space will appear in the fourth partition of your VM's disk. You can
turn this extra space into a usable file system by logging into your
VM and doing:

@codeblock{
mynode> sudo mkdir /dirname
mynode> sudo /usr/local/etc/emulab/mkextrafs.pl /dirname
}

where @tt{dirname} is the directory you want your newly-formatted file
system to be mounted.

@code-sample["geni-lib-xen-cores-ram.py"]

@subsection[#:tag "xen-hvm"]{Setting HVM Mode}

By default, all Xen VMs are @hyperlink["https://wiki.xen.org/wiki/Paravirtualization_(PV)"]{paravirtualized}.
If you need @hyperlink["https://wiki.xen.org/wiki/Xen_Project_Software_Overview#HVM_and_its_variants_.28x86.29"]{hardware virtualization}
instead, you must set a @tt{XEN_FORCE_HVM} node attribute, as shown in
this example:

@code-sample["geni-lib-single-hvm.py"]

You can set this attribute only for dedicated-mode VMs.  Shared VMs are
available only in paravirtualized mode.


@section[#:tag "docker-containers"]{Docker Containers}

@(tb) supports experiments that use Docker containers as virtual nodes.
In this section, we first describe how to build simple profiles that
create Docker containers, and then demonstrate more advanced features.
The @(tb)-Docker container integration has been designed to enable easy
image onboarding, and to allow users to continue to work naturally with
the standard Docker API or CLI.  However, because @(tb) is itself an
orchestration engine, it does not support any of the Docker orchestration
tools or platforms, such as Docker Swarm.

You can request a @(tb) Docker container in a geni-lib script like this:

@codeblock{
import geni.portal as portal
import geni.rspec.pg as rspec
request = portal.context.makeRequestRSpec()
node = request.DockerContainer("node")
}

You can use the returned @tt{node} object
(a @(geni-lib "geni.rspec.igext.DockerContainer" "DockerContainer")
instance) similarly to other kinds of node objects, like
@(geni-lib "geni.rspec.pg.RawPC" "RawPC") or
@(geni-lib "geni.rspec.igext.XenVM" "XenVM").  However, Docker nodes have
@seclink["docker-member-variables"]{several custom member variables} you can
set to control their behavior and Docker-specific features.  We
demonstrate the usage of these member variables in the following subsections
and @seclink["docker-member-variables"]{summarize them at the end of this section}.

@subsection[#:tag "docker-basic-examples"]{Basic Examples}

@code-sample["geni-lib-single-shared-container.py"]

It is easy to extend this profile slightly to allocate 10 containers in
a LAN, and to switch them to @seclink["docker-shared-mode"]{dedicated
mode}.  Note that in this case, the
@(geni-lib "geni.rspec.pg.Node.exclusive" "exclusive")
member variable is not specified, and it defaults to @tt{False}):

@code-sample["geni-lib-docker-lan.py"]

Here is a more complex profile that creates 20 containers, binds 10 of
them to a physical host machine of a particular type, and binds the
other 10 to a second machine of the same type:

@code-sample["geni-lib-docker-vhost-lan.py"]

@subsection[#:tag "docker-disk-images"]{Disk Images}

Docker containers use a different
@hyperlink["https://docs.docker.com/engine/reference/builder/"]{disk
image format} than @seclink["disk-images"]{@(tb) physical machines or
Xen virtual machines}, which means that you cannot use the same images
on both a container and a raw PC.  However, @(tb) supports native Docker
images in several modes and workflows.  @(tb) hosts a private Docker
registry, and the standard @(tb) image-deployment and -capture
mechanisms support capturing container disk images into it.  @(tb) also
supports the use of externally hosted, unmodified Docker images and
Dockerfiles for image onboarding and dynamic image creation.  Finally,
since some @(tb) features require in-container support (e.g., user
accounts, SSH pubkeys, syslog, scripted program execution), we also
provide an optional @seclink["docker-augmentation"]{automated process},
called augmentation, through which an external image can be customized
with the @(tb) software and dependencies.

@(tb) supports both augmented and unmodified Docker images, but some
features require augmentation (e.g., that the @(tb) client-side software
is installed and running in the container).  Unmodified images support
these @(tb) features: network links, link shaping, remote access, remote
storage (e.g. remote block stores), and image capture.  Unmodified
images do not support user accounts, SSH pubkeys, or scripted program
execution.

@;{
First, @(tb) hosts a private Docker registry, and the standard @(tb) image-loading and -capturing mechanisms have been modified to use it, so you can create custom images based on our standard Docker system images.  When you capture an image, it will be stored in our private registry.so you can load and store custom
images in it
and import external Docker images from other registries.  You can
use unmodified external images, or you can tell @(tb) to
@seclink["docker-augmentation"]{@italic{augment} them} automatically
with the Emulab software so that they can more broadly support the @(tb)
feature set (unaugmented images do support some @(tb) features; see
below for more detail).  Finally, you can modify and capture new
versions of your disk images using the standard @(tb) image-capturing
process.  Or, you can use manual @tt{docker commit} invocations on the
physical host node to capture your own images, and manually push them to
other registries.  Finally, you can use your @(tb) credentials to login
to the @(tb) private Docker registry, and pull (download) your images
for use elsewhere.
}

@(tb)'s disk image naming and versioning scheme is slightly different
from Docker's content-addressable model.  A @(tb) disk image is
identified by a project and name tuple (typically encoded as a URN), or
by a UUID, as well as a version number that starts at @tt{0}.  Each time
you capture a new version of an image, the image's version number is
incremented by one.  @(tb) does not support the use of arbitrary
alphanumeric tags to identify image versions, as Docker does.

Thus, when you capture an @(tb) disk image of a @(tb) Docker container,
and give it a name, the local @(tb) registry will contain an image
(repository) of that name, in the project (and group---nearly always the
project name) your experiment was created within---and thus the full
image name is @tt{<project-name>/<group-name>/<image-name>}.  The tags
within that repository correspond to the integer version numbers of the
@(tb) disk image.  For example, if you have created an @(tb) image named
@tt{docker-my-research} in project @tt{myproject}, and you have created
three versions (@tt{0}, @tt{1}, @tt{2}) and want to pull the latest
version (@tt{2}) to your computer, you could run this command:

@codeblock{
docker pull ops.emulab.net:5080/myproject/myproject/docker-my-research:2
}

You will be prompted for username and password; use your @(tb) credentials.

The following code fragment creates a Docker container that uses a
standard @(tb) Docker disk image, @tt{docker-ubuntu16-std}.  This image
is based on the
@hyperlink["https://hub.docker.com/_/ubuntu/"]{@tt{ubuntu:16.04}} Docker
image, with the Emulab client software installed (meaning it is
@seclink["docker-augmentation"]{augmented}) along with dependencies and
other utilities.

@code-sample["geni-lib-docker-stdimage.py"]

@subsection[#:tag "docker-ext-images"]{External Images}

@(tb) supports the use of publicly accessible Docker images in other
registries.  It does not currently support username/password access to
images.  By default, if you simply specify a repository and tag, as in
the example below, @(tb) assumes the image is in the standard Docker
registry; but you can instead specify a complete URL pointing to a
different registry.

@code-sample["geni-lib-docker-extimage.py"]

By default, @(tb) assumes that an external, non-augmented image does not run
its own @tt{sshd} to support remote login.  Instead, it facilitates
remote access to a container by running an alternate @tt{sshd} on the
container host and executing a shell (by default @tt{/bin/sh}) in the
container associated with a specific port (the port in the @tt{ssh} URL
shown on your experiment's page).  See
@seclink["docker-remote-access"]{the section on remote access} below for
more detail.

@subsection[#:tag "docker-dockerfiles"]{Dockerfiles}

You can also create images dynamically (at experiment runtime) by
specifying a @tt{Dockerfile} for each container.  Note that if multiple
containers hosted on the same physical machine reference the same
@tt{Dockerfile}, the dynamically built image will only be built once.
Here is a simple example of a @tt{Dockerfile} that builds @tt{httpd}
from source:

@code-sample["geni-lib-docker-dockerfile.py"]

You should not assume you have access to the image build environment
(you only do if your container(s) are running in
@seclink["docker-shared-mode"]{dedicated mode})---you should test your
@tt{Dockerfile} on your local machine first to ensure it works.
Moreover, your @tt{Dockerfile} will have an empty context directory, so
any file resources it requires must be downloaded during the image
build, from instructions in the @tt{Dockerfile}.

@;{
REORG according to this outline:

disk images
  * external images
  * images from Dockerfiles
  * what features purely external images support
  * standard images
  * augmentation, levels, features
emulabization, and those geni-lib param/values
  * levels, features
}

@subsection[#:tag "docker-augmentation"]{Augmented Disk Images}

The primary use case that Docker supports involves a large collection of
containers, potentially spanning many machines, in which each container
runs a single application (one or more processes launched by a single
parent) that provides a specific service.  In this model, container
images may be tailored to support only their single service,
unencumbered by extra software.  Many Docker images do not include an
init daemon to launch and monitor processes, or even a basic set of
user-space tools and libraries.
@;{
The Docker community does not recommend the use of whole-OS-like
containers that run an init and other common supporting services.
}

@(tb)-based experimentation requires more than the ability to run a
single service per node.  Within an experiment, it is beneficial for
each node to run basic OS services (e.g., syslogd and sshd) to support
activities such as debugging, interactive exploration, logging, and
more.  It is beneficial for each node to run a suite of @(tb)-specific
services to configure the node, conduct in-node monitoring, and automate
experiment deployment and control (e.g., launch programs or dynamically
emulate link failures).  This environment requires a full-featured init
daemon and a common, basic set of user-space software and libraries.

To enable experiments that use existing Docker images and seamlessly
support the @(tb) environment, @(tb) supports automatic
@italic{augmentation} of those images.  When requested, @(tb) pulls an
existing image, builds both @tt{runit} (a simple, minimal @tt{init}
daemon) and the @(tb) client toolchain against it in temporary
containers, and creates a new image with the just-built @tt{runit} and
@(tb) binaries, scripts, and dependencies.  Augmented images are
necessary to fully support some @(tb) features (specifically, the
creation of user accounts, installation of SSH pubkeys, and execution of
startup scripts).  Other @(tb) features (such as experiment network
links and block storage) are configured outside the container at startup
and do not require augmentation.

During augmentation, @(tb) modifies Docker images to run an init system
rather than a single application.  @(tb) builds and packages (in
temporary containers with a build toolchain) and installs (in the final
augmented image) @tt{runit} and sets it as the augmented image’s
@tt{ENTRYPOINT}.  @(tb) creates @tt{runit} services to emulate existing
@tt{ENTRYPOINT} and/or @tt{CMD} instructions from the original image.
The @(tb) client-side software is also built and installed.

@subsection[#:tag "docker-remote-access"]{Remote Access}

@(tb) provides remote access via @tt{ssh} to each node in an experiment.
If your container is running an @tt{sshd} inside, then @tt{ssh} access
is straightfoward: @(tb) allocates a high-numbered port on the container
host machine, and redirects (@tt{DNAT}s) that port to the private,
unrouted control network IP address assigned to the container. (@(tb)
containers typically receive private addresses that are only routed on
the @(tb) control network to increase experiment scalability.)  We refer
to this style of @tt{ssh} remote login as @tt{direct}.

However, most unmodified, unaugmented Docker images do not run
@tt{sshd}.  To trivially allow remote access to containers running such
images, @(tb) runs a proxy @tt{sshd} in the host context on the
high-numbered ports assigned to each container, and turns
successfully authenticated incoming connections into invocations of
@tt{docker exec <container> /bin/sh}.  We refer to this style of
@tt{ssh} remote login as @tt{exec}.

You can force the @tt{ssh} style you prefer on a per-container
basis---and if using the @tt{exec} style, you can also choose a specific
shell to execute inside the container.  For instance:

@code-sample["geni-lib-docker-ssh-exec.py"]

@subsection[#:tag "docker-console"]{Console}

Although Docker containers do not have a serial console, as do most
@(tb) physical machines, they can provide a @tt{pty} (pseudoterminal)
for interaction with the main process running in the container.  @(tb)
attaches to these streams via the Docker daemon and proxies them into
the @(tb) console mechanism.  Thus, if you click the "Console" link for
a container on your experiment status page, you will see output from the
container and be able to send it input.  (Each time you close and
reopen the console for a container, your console will see the entire
console log kept by the Docker daemon, so there may be a flood of
initial output if your container has been running for a long time and
producing output.)

@subsection[#:tag "docker-entrypoint-cmd"]{@tt{ENTRYPOINT and CMD}}

If you run an @seclink["docker-augmentation"]{augmented} Docker image in a container,
recall that the augmentation process overrides the @tt{ENTRYPOINT} and/or
@tt{CMD} instruction(s) the image contained, if any.  Changing the
@tt{ENTRYPOINT} means that the augmented image will not run the command
that was specified for the original image.  Moreover, @tt{runit} must be
run as root, but the image creator may have specified a different
@tt{USER} for processes that execute in the container.  To fix these
problems, @(tb) emulates the original (or runtime-specific) @tt{ENTRYPOINT} as an runit service
and handles several related Dockerfile settings as well: @tt{CMD},
@tt{WORKDIR}, @tt{ENV}, and @tt{USER}.  The emulation preserves the
semantics of these settings (see @link["https://docs.docker.com/engine/reference/builder/#understand-how-cmd-and-entrypoint-interact"]{Docker's reference on interaction between @tt{ENTRYPOINT} and @tt{CMD}}), with the exception that the user-specified
@tt{ENTRYPOINT} or @tt{CMD} is not executed as @tt{PID 1}.  Only the
@tt{ENTRYPOINT} and @tt{CMD} processes run as @tt{USER}; processes
started from outside the container via @tt{docker exec} run as root.

You can customize the @tt{ENTRYPOINT} and @tt{CMD} for each container you create by setting
the @(geni-lib "geni.rspec.igext.DockerContainer.docker_entrypoint" "docker_entrypoint")
and the @(geni-lib "geni.rspec.igext.DockerContainer.docker_cmd" "docker_cmd")
member variables of a @tt{geni-lib}
@(geni-lib "geni.rspec.igext.DockerContainer" "DockerContainer")
object instance.

The @tt{runit} service that performs this emulation is named
@tt{dockerentrypoint}.  If this service exits, it is not automatically
restarted, unlike most @tt{runit} services.  You can start it again by
running @tt{sv start dockerentrypoint} within a container.  You can also
@link["http://smarden.org/runit/"]{browse the @tt{runit} documentation}
for more examples of interacting with @tt{runit}.

This emulation process is complicated, so ifyou suspect problems, there
are logfiles written in each container that may help.  The @tt{stdout}
and @tt{stderr} output from the @tt{ENTRYPOINT} and @tt{CMD} emulation
is logged to @tt{/var/log/entrypoint.log}, and additional debugging
information is logged in @tt{/var/log/entrypoint-debug.log}.

@subsection[#:tag "docker-shared-mode"]{Shared Containers}

In @(tb), Docker containers can be created in @italic{dedicated} or
@italic{shared} mode.  In dedicated mode, containers run on physical
nodes that are reserved to a particular experiment, and you have
root-level access to the underlying physical machine.  In shared mode,
containers run on physical machines that host containers from
potentially many experiments, and users do not have access to the
underlying physical machine.

@subsection[#:tag "docker-privilege"]{Privileged Containers}

Docker allows containers to be @link["https://docs.docker.com/engine/reference/run/#runtime-privilege-and-linux-capabilities"]{privileged or unprivileged}: a privileged
container has administrative access to the underlying host.  @(tb)
allows you to spawn privileged containers, but only on dedicated
container hosts.  Moreover, you should only do this when absolutely
necessary, because a hacked privileged container can effectively take
over the physical host, and access and control other containers.  To
make a container privileged, set the @(geni-lib "geni.rspec.igext.DockerContainer.docker_privileged" "docker_privileged") member
variable of a @tt{geni-lib}
@(geni-lib "geni.rspec.igext.DockerContainer" "DockerContainer")
object instance.

@subsection[#:tag "docker-blockstore"]{Remote Blockstores}

You can mount @(tb) remote blockstores in Docker containers --- if they
were formatted with a Linux-mountable filesystem.  Here is an example:

@code-sample["geni-lib-docker-blockstore.py"]

@(tb) does not support mapping raw devices (or in the case of @(tb)
remote blockstores, virtual ISCSI-backed devices) into containers.

@subsection[#:tag "docker-temp-blockstore"]{Temporary Block Storage}

You can mount @(tb) temporary blockstores in Docker containers.  Here is
an example that places an 8 GB filesystem in a container at
@tt{/mnt/tmp}:

@code-sample["geni-lib-docker-temp-storage.py"]

@subsection[#:tag "docker-member-variables"]{@tt{DockerContainer} Member Variables}

The following table summarizes the
@(geni-lib "geni.rspec.igext.DockerContainer" "DockerContainer")
class member variables.  You can find more detail either in the sections
above, or by browsing the 
@(geni-lib "geni.rspec.igext.DockerContainer" "source code documentation").


@(tabular #:style 'boxed #:sep (hspace 3) (list
  (list (geni-lib "geni.rspec.igext.DockerContainer.cores" "cores")
    "The amount (integer) of virtual CPUs this container should receive; approximated using Docker's CpuShares and CpuPeriod options.")
  (list (geni-lib "geni.rspec.igext.DockerContainer.ram" "ram")
    "The amount (integer) of memory this container should receive.")
  (list (geni-lib "geni.rspec.pg.Node.disk_image" "disk_image")
    (list "The disk image this node should run.  See " @seclink["docker-disk-images"]{the section on Docker disk images} " below."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_ptype" "docker_ptype")
    (list "The physical node type on which to instantiate the container. Types are cluster-specific; see " @seclink["hardware"]{the hardware chapter} "."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_extimage" "docker_extimage")
    (list "An external Docker image (repo:tag) to load on the container; see " @seclink["docker-ext-images"]{the section on external Docker images} "."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_dockerfile" "docker_dockerfile")
    (list "A URL that points to a Dockerfile from which an image for this node will be created; see " @seclink["docker-dockerfiles"]{the section on Dockerfiles} "."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_tbaugmentation" "docker_tbaugmentation")
    (list "The requested testbed augmentation level: must be one of " @tt{full, buildenv, core, basic, none} ".  See " @seclink["docker-augmentation"]{the section on Docker image augmentation} "."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_tbaugmentation_update" "docker_tbaugmentation_update")
    (list "If the image has already been augmented, should we update it " @tt{True} " or not " @tt{False} "."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_ssh_style" "docker_ssh_style")
    (list "Specify what happens when you ssh to your node; must be either " @tt{direct} " or " @tt{exec} ".  See " @seclink["docker-remote-access"]{the section on remote access} "."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_exec_shell" "docker_exec_shell")
    (list "The shell to run if the value of " (geni-lib "geni.rspec.igext.DockerContainer.docker_ssh_style" "docker_ssh_style") " is " @tt{direct} "; ignored otherwise."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_entrypoint" "docker_entrypoint")
    (list "Change/set the Docker " @tt{ENTRYPOINT} " option for this container; see " @seclink["docker-entrypoint-cmd"]{the section on Docker @tt{ENTRYPOINT} and @tt{CMD} handling} "."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_cmd" "docker_cmd")
    (list "Change/set the Docker " @tt{CMD} " option for this container; see " @seclink["docker-entrypoint-cmd"]{the section on Docker @tt{ENTRYPOINT} and @tt{CMD} handling} "."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_env" "docker_env")
    (list "Add or override environment variables to a container.  The value to this attribute should be either a newline-separated list of variable assignments, or one or more variable assignments on a single line.  If the former, we do not support escaped newlines, unlike the Docker " @link["https://docs.docker.com/engine/reference/builder/#env"]{@tt{ENV} instruction} "."))
  (list (geni-lib "geni.rspec.igext.DockerContainer.docker_privileged" "docker_privileged")
    (list "Set this member variable " @tt{True} " to make the container privileged.  See " @seclink["docker-privilege"]{the section on Docker privileged containers} "."))
))

@;{

@subsection[#:tag "docker-exposing-ports"]{Exposing Services on Ports}



@subsection[#:tag "docker-misc"]{Other Settings}

network types (bridge, macvlan)
disk layer driver (lvm, aufs)

@subsection[#:tag "docker-unsupported"]{Unsupported @(tb) Features}

unsupported features:
  docker cross-container volume mount
  swarm/compose service referencing
}

"""An example of a Docker container that mounts a remote blockstore."""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as ig

request = portal.context.makeRequestRSpec()
node = request.DockerContainer("node")
# Create an interface to connect to the link from the container to the
# blockstore host.
myintf = 
# Create the blockstore host.
bsnode = ig.RemoteBlockstore("bsnode","/mnt/blockstore")
# Map your remote blockstore to the blockstore host
bsnode.dataset = \
    "urn:publicid:IDN+emulab.net:emulab-ops+ltdataset+johnsond-bs-foo"
bsnode.readonly = False
# Connect the blockstore host to the container.
bslink = pg.Link("bslink")
bslink.addInterface(node.addInterface("ifbs0"))
bslink.addInterface(bsnode.interface)

portal.context.printRequestRSpec()

#lang scribble/manual
@(require racket/date)
@(require "defs.rkt")

@title[#:version apt-version
       #:date (date->string (current-date))]{The Emulab Manual}

@author[
     "Eric Eide" "Robert Ricci" "Jacobus (Kobus) Van der Merwe" "Leigh Stoller" "Kirk Webb" "Jon Duerig" "Gary Wong" "Keith Downie" "Mike Hibler" "David Johnson"
]

@;{
@italic[(if (equal? (doc-mode) 'pdf)
    (list "The HTML version of this manual is available at " (hyperlink apt-doc-url apt-doc-url))
    (list "This manual is also available as a " (hyperlink "http://docs.emulab.net/manual.pdf" "PDF")))]
}

Emulab is a network testbed, giving researchers a wide range of
environments in which to develop, debug, and evaluate their
systems. The name Emulab refers both to a facility and to a software
system. The primary Emulab installation is run by the
@hyperlink["http://www.flux.utah.edu"]{Flux Research Group}, part of the
@hyperlink["http://www.cs.utah.edu/"]{School of Computing} at the
@hyperlink["http://www.utah.edu/"]{University of Utah}.
There are also installations of the Emulab software at more than two
dozen sites around the world, ranging from testbeds with a handful of
nodes up to testbeds with hundreds of nodes. Emulab is widely used by
computer science researchers in the fields of networking and
distributed systems. It is also designed to support education, and has
been used to teach classes in those fields.

Emulab is a public facility, available without charge to most
researchers worldwide. If you are unsure if you qualify for use,
please see our policies document, or ask us. If you think you qualify,
you can apply to start a new project.

@table-of-contents[]

@include-section["getting-started.scrbl"]
@include-section["users.scrbl"]
@include-section["repeatable-research.scrbl"]
@include-section["creating-profiles.scrbl"]
@include-section["basic-concepts.scrbl"]
@include-section["emulab-transition.scrbl"]
@include-section["reservations.scrbl"]
@include-section["geni-lib.scrbl"]
@include-section["virtual-machines.scrbl"]
@include-section["advanced-topics.scrbl"]
@include-section["emulab-hardware.scrbl"]
@include-section["planned.scrbl"]
@include-section["openstack-tutorial.scrbl"]
@include-section["getting-help.scrbl"]
